<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    //
     public function create(Request $request)
    {
        return User::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        return $user;
    }

    public function delete(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return 204;
    }
    
    public function list(Request $request){
        //$user_query = User::all();
        if($request->sortBy && in_array($request->sortBy, ['email', 'phone', 'fullname', 'age', 'created_at'])){
            $sortBy = $request->sortBy;
        }else{
            $sortBy = 'id';
        }
        
        if($request->sortOrder && in_array($request->sortOrder, ['asc', 'desc'])){
            $sortOrder = $request->sortOrder;
        }else{
            $sortOrder = 'desc';
        }
        
        $users = User::orderBy($sortBy, $sortOrder)->get();
        
        
        if($request->email){
            $users->where('email', 'LIKE', "%{$request->email}%");
        }
        
        if($request->phone){
          
            $users->where('phone', 'LIKE', "%{$request->phone}%");
        }
        
        //$users->orderBy($sortBy, $sortOrder)->get();
        
        //$users->orderBy($sortBy, $sortOrder)->get();
        return response()->json([
           'data' => $users], 200 
        );
    }
}
